//
//  CardDb.swift
//  CardScan
//
//  Created by Maksim Avksentiev on 2/18/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation
import CoreData

extension CardDb {
    
    class func getAll(in context: NSManagedObjectContext) -> [CreditCard] {
        
        let array: [CardDb] = CoreData.manager.read(context)
        
        return array.compactMap{CreditCard(cardDb: $0)}
    }
}
