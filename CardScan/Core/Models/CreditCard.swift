//
//  CreditCard.swift
//  CardScan
//
//  Created by Maksim Avksentiev on 2/18/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

struct CreditCard {
    
    let id: String
    let name: String
    let number: String
    let expireDate: String
}

extension CreditCard {
    
    init(cardDb: CardDb) {
        
        self.init(id: cardDb.id ?? "", name: cardDb.name ?? "", number: cardDb.number ?? "", expireDate: cardDb.expireDate ?? "")
    }
}
