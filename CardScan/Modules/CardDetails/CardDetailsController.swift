//
//  CardDetailsController.swift
//  CardScan
//
//  Created by Maksim Avksentiev on 2/18/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit
import PayCardsRecognizer

class CardDetailsController: BaseController {
    
    @IBOutlet weak var holderNameTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var expireDateTextField: UITextField!
    
    var result: PayCardsRecognizerResult?
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.configure()
    }
}

//MARK: - Configuration
extension CardDetailsController {
    
    fileprivate func configure() {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAction))
        
        self.holderNameTextField.text = self.result?.recognizedHolderName
        self.numberTextField.text = self.result?.recognizedNumber?.format(" ")
        
        if let month = self.result?.recognizedExpireDateMonth, let year = self.result?.recognizedExpireDateYear {
            
            self.expireDateTextField.text = String(format: "%@/%@", month, year)
        }
    }
}

//MARK: - Actions
extension CardDetailsController {
    
    @objc func addAction() {
        
        let name = self.holderNameTextField.text ?? ""
        let number = self.numberTextField.text ?? ""
        let date = self.expireDateTextField.text ?? ""
        
        if name.isEmpty || number.isEmpty || date.isEmpty {
            
            self.presentAlert()
        } else {
            
            let cardDb: CardDb? =  CoreData.manager.createOrUpdate(CoreData.manager.currentThreadContext(), primary: ["number": number, "expireDate": date], secondary: ["id": String(Date().timeIntervalSince1970), "name": name])
            
            if cardDb != nil {
                
                let card = CreditCard(cardDb: cardDb!)
                NotificationCenter.default.post(name: NotificationNames.addCard, object: card)
            }
            
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func presentAlert() {
        
        let alert = UIAlertController(title: NSLocalizedString("Fill data", comment: ""), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Dismiss", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
