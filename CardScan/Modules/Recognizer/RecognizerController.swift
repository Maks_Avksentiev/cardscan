//
//  RecognizerController.swift
//  CardScan
//
//  Created by Maksim Avksentiev on 2/18/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit
import PayCardsRecognizer

class RecognizerController: BaseController {
    
    @IBOutlet weak var recognizerContainer: UIView!
    
    lazy var activityView: UIBarButtonItem = {
        
        let activityView = UIActivityIndicatorView(style: .gray)
        activityView.startAnimating()
        return UIBarButtonItem(customView: activityView)
    }()
    
    var recognizer: PayCardsRecognizer!

    //MARK: - LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.recognizer = PayCardsRecognizer(delegate: self, resultMode: .async, container: recognizerContainer, frameColor: .green)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.recognizer.startCamera()
        self.recognizerContainer.subviews.first?.subviews.filter({$0 is UIButton}).first?.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
       
        super.viewDidDisappear(animated)
        
        self.recognizer.stopCamera()
        self.navigationItem.rightBarButtonItem = nil
    }
}

//MARK: - PayCardsRecognizerPlatformDelegate
extension RecognizerController: PayCardsRecognizerPlatformDelegate {
    
    func payCardsRecognizer(_ payCardsRecognizer: PayCardsRecognizer, didRecognize result: PayCardsRecognizerResult) {
        
        if result.isCompleted {
            
            let controller = CardDetailsController()
            controller.result = result
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            self.navigationItem.rightBarButtonItem = self.activityView
        }
    }
}
