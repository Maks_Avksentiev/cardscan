//
//  WalletController.swift
//  CardScan
//
//  Created by Maksim Avksentiev on 2/18/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class WalletController: BaseController {

    @IBOutlet weak var tableView: UITableView!
    
    var cards = [CreditCard]()
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addCard))
        
        self.configure()
    }

    fileprivate func configure() {
        
        self.load()
        self.registerNotifications()
        self.tableView.allowsSelection = false
        self.tableView.allowsSelectionDuringEditing = false
        
        self.tableView.register(CardCell.nib(), forCellReuseIdentifier: CardCell.reuseIdentifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    fileprivate func load() {
        
        self.cards = CardDb.getAll(in: CoreData.manager.currentThreadContext())
    }
    
    fileprivate func registerNotifications() {
        
        NotificationCenter.default.addObserver(forName: NotificationNames.addCard, object: nil, queue: OperationQueue()) { (notification) in
            
            guard let card = notification.object as? CreditCard else {
                return
            }
            
            self.cards.insert(card, at: 0)
            DispatchQueue.main.async {
                self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            }
        }
    }
}

//MARK: - UITableViewDelegate
extension WalletController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            let card = self.cards[indexPath.row]
            let _: CardDb? = CoreData.manager.delete(CoreData.manager.currentThreadContext(), parametrs: ["number": card.number, "expireDate": card.expireDate, "id": card.id, "name": card.name])
            self.cards.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .left)
        }
    }
}

//MARK: - UITableViewDataSource
extension WalletController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CardCell.reuseIdentifier, for: indexPath) as? CardCell else {
            
            return UITableViewCell()
        }
        
        cell.set(card: self.cards[indexPath.row])
        
        return cell
    }
}

//MARK: - Actions
extension WalletController {
 
    @objc func addCard() {
        
        self.navigationController?.pushViewController(RecognizerController(), animated: true)
    }
}
