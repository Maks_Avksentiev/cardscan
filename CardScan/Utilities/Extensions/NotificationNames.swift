//
//  NotificationNames.swift
//  CardScan
//
//  Created by Maksim Avksentiev on 2/18/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

struct NotificationNames {
    
    static let addCard = Notification.Name("AddCardNotification")
}
