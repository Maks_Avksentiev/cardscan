//
//  String+Format.swift
//  CardScan
//
//  Created by Maksim Avksentiev on 2/18/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

extension String {
    
    public func format(_ seprator: String) -> String {
        
        var dst = [String]()
        
        self.enumerated().forEach({
            dst.append(String($0.element))
            ($0.offset % 4 == 3) ? (dst.append(seprator)) : ()
        })
        
        return dst.joined(separator: "")
    }
    
}
