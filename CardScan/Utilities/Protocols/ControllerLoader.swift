//
//  ControllerLoader.swift
//  CardScan
//
//  Created by Maksim Avksentiev on 2/18/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

import UIKit

protocol ControllerLoader where Self: UIViewController {
    
    static var identifier: String {get}
    
    init()
    
    func load()
    func configure()
}

extension ControllerLoader {
    
    init() {
        
        self.init(nibName: Self.identifier, bundle: Bundle.main)
        self.load()
    }
    
    init?(coder aDecoder: NSCoder) {
        
        self.init(coder: aDecoder)
    }
    
    func configure() {}
    
    func load() {}
}
