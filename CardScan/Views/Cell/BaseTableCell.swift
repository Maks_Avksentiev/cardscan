//
//  BaseTableCell.swift
//  CardScan
//
//  Created by Maksim Avksentiev on 2/18/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class BaseTableCell: UITableViewCell {}

//MARK: - NibLoadable
extension BaseTableCell: NibLoadable {}

//MARK: - ReusableView
extension BaseTableCell: ReusableView {}
