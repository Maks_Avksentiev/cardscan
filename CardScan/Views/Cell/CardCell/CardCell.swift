//
//  CardCell.swift
//  CardScan
//
//  Created by Maksim Avksentiev on 2/18/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class CardCell: BaseTableCell {

    @IBOutlet weak var holderNameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var expireDateLabel: UILabel!
    
    //MARK: - Configure
    func set(card: CreditCard) {
        
        self.holderNameLabel.text = card.name
        self.numberLabel.text = card.number
        self.expireDateLabel.text = card.expireDate
    }
}


